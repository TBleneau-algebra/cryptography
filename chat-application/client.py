# System import
import socket
import select
import signal
import base64

# Library import
from lib.communication import *
from lib.crypto.keys import *
from lib.crypto.signature import *
from lib.crypto.utils import *
from lib.crypto.decryption import *
from lib.crypto.encryption import *

# Constant import
from constant.input import *
from constant.error import *
from constant.message import *
from constant.color import *

# User's socket
_socket_user = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# List of users connected via socket
_records_name = {}
_records_public_key = {}

# User's private_key / public_key
private_key = private_key_generation()
public_key = public_key_generation(private_key)

public_key_bytes = public_key_to_bytes(public_key)
private_key_bytes = private_key_to_bytes(private_key)

public_key_signed = signature_creation(private_key, public_key_bytes)
private_key_signed = signature_creation(private_key, private_key_bytes)


def signal_handler(sig, frame):
    print_message(ERROR_DISCONNECTED, color=COLOR_RED)
    sys.exit()


def initialisation():
    try:
        host = input(INPUT_SERVER_ADDRESS)
        port = input(INPUT_SERVER_PORT)
        name = input(INPUT_USERNAME)

        if not port.isnumeric():
            print_message(ERROR_SERVER_PORT, color=COLOR_RED)
            sys.exit()
        port = int(port)
    except Exception as e:
        print_message(ERROR_CONFIGURATION, color=COLOR_RED, e=e)
        sys.exit()

    return host, port, name


def connection(host, port):
    try:
        _socket_user.settimeout(2)
        _socket_user.connect((host, port))
    except Exception as e:
        print_message(ERROR_SERVER_CONNECTION, color=COLOR_RED, e=e)
        sys.exit()


def validation(name):
    message = create_message_from_info(string_to_bytes(name), public_key_bytes, public_key_signed)
    send(_socket_user, message)

    data_received = receive(_socket_user)
    data_received = decrypt_message(private_key, data_received)
    server_name, message, signature = get_info_from_message(data_received)

    server_public_key = read_public_key(message)

    if not signature_validation(server_public_key, message, signature):
        send_message(_socket_user, server_public_key, private_key,
                     server_name, string_to_bytes(MESSAGE_INCOMPLETE))
        return None

    (i, p) = _socket_user.getpeername()
    _records_name[(i, p)] = server_name
    _records_public_key[(i, p)] = server_public_key

    send_message(_socket_user, server_public_key, private_key,
                 server_name, string_to_bytes(MESSAGE_COMPLETE))
    return bytes_to_string(server_name)


def main():
    if len(sys.argv) != 1:
        print_message(ERROR_ARGUMENT, color=COLOR_RED)
        sys.exit()

    signal.signal(signal.SIGINT, signal_handler)
    host, port, name = initialisation()

    connection(host, port)
    server_name = validation(name)
    if server_name is not None:
        while 1:
            socket_list = [sys.stdin, _socket_user]
            rlist, wlist, error_list = select.select(socket_list, [], [])

            for _socket_it in rlist:
                if _socket_it == _socket_user:

                    message_received = receive(_socket_it)
                    if message_received is None or message_received == b'':
                        print_message(ERROR_DISCONNECTED, color=COLOR_RED)
                        sys.exit()

                    message_received = decrypt_message(private_key, message_received)
                    user, message, signature = get_info_from_message(message_received)

                    if bytes_to_string(message) == ERROR_SERVER_STOPPED:
                        print_message(ERROR_DISCONNECTED, color=COLOR_RED)
                        sys.exit()
                    else:
                        if bytes_to_string(user) == server_name:
                            print_message("\n > " + bytes_to_string(user), color=COLOR_BLUE, end=" ")
                        else:
                            print_message("\n > " + bytes_to_string(user), color=COLOR_PURPLE, end=" ")
                        print_message(bytes_to_string(message), end="")
                        print_prompt(MESSAGE_PROMPT_ME)
                else:
                    (i, p) = _socket_user.getpeername()
                    user_input = sys.stdin.readline()
                    user_input = user_input.replace("\n", '')

                    send_message(_socket_user, _records_public_key[(i, p)], private_key,
                                 string_to_bytes(name), string_to_bytes(user_input))
                    print_prompt(MESSAGE_PROMPT_ME)


if __name__ == "__main__":
    main()
