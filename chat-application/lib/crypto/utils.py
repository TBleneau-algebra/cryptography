# System import
import base64
import sys

# Constant import
from constant.error import *
from lib.print import *


def string_to_bytes(value):
    try:
        rep = bytes(value, encoding="utf-8")
    except Exception as e:
        print_message(ERROR_CONVERSION_BYTES, color=COLOR_RED, e=e)
        sys.exit()
    return rep


def bytes_to_b64(value):
    try:
        rep = base64.b64encode(value)
    except Exception as e:
        print_message(ERROR_CONVERSION_B64, color=COLOR_RED, e=e)
        sys.exit()
    return rep


def bytes_to_string(value):
    try:
        rep = value.decode('utf-8')
    except Exception as e:
        print_message(ERROR_CONVERSION_STRING, color=COLOR_RED, e=e)
        sys.exit()
    return rep


def b64_to_bytes(value):
    try:
        rep = base64.b64decode(value)
    except Exception as e:
        print_message(ERROR_CONVERSION_BYTES, color=COLOR_RED, e=e)
        sys.exit()
    return rep


def get_info_from_message(message):
    reponses = message.split(b':')

    if len(reponses) == 3:
        user = b64_to_bytes(reponses[0])
        public_key = b64_to_bytes(reponses[1])
        signature = b64_to_bytes(reponses[2])
    else:
        return None, None, None
    return user, public_key, signature


def create_message_from_info(user, message, signature):
    message = bytes_to_b64(user) + b':' + bytes_to_b64(message) + b':' + bytes_to_b64(signature)
    return message
