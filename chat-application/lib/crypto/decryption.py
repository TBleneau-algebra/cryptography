# System import
import math
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

# Library import
from lib.print import *

# Constant import
from constant.error import *
from constant.color import *


def decrypt_message(private_key, message):
    try:
        original_message = b''
        index = 0
        size = int(math.ceil(private_key.key_size / 8.0))

        while size <= len(message):
            original_message += private_key.decrypt(
                message[index:size],
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            )
            index = size
            size += int(math.ceil(private_key.key_size / 8.0))

        original_message = original_message
        return original_message
    except Exception as e:
        print_message(ERROR_DECRYPTION, color=COLOR_RED, e=e)
        return None
