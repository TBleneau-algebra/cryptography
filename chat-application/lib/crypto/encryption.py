# System import
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

# Library import
from lib.print import *

# Constant import
from constant.error import *
from constant.color import *


def encrypt_message(public_key, message):
    try:
        encrypted = b''
        index = 0
        size = 190

        if type(message) is not bytes:
            message = bytes(message.encode('ascii'))
        while index <= len(message):
            encrypted += public_key.encrypt(
                message[index:size],
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            )
            index = size
            size += 190

        return encrypted
    except Exception as e:
        print_message(ERROR_ENCRYPTION, color=COLOR_RED, e=e)
        return None
