# System import
import hashlib
import base64
import sys
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.exceptions import InvalidSignature

# Library import
from lib.print import *

# Constant import
from constant.error import *
from constant.color import *


def signature_creation(private_key, message):
    try:
        if type(message) is not bytes:
            message = bytes(message.encode('ascii'))

        pre_hashed = hashlib.sha256(message).hexdigest()
        sig = private_key.sign(
            bytes(pre_hashed.encode('ascii')),
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH),
            hashes.SHA256()
        )

        return sig
    except Exception as e:
        print_message(ERROR_SIGNATURE_CREATION, color=COLOR_RED, e=e)
        sys.exit()


def signature_validation(public_key, message, signature):
    try:
        if type(message) is not bytes:
            message = bytes(message.encode('ascii'))
        pre_hashed = hashlib.sha256(message).hexdigest()

        if type(signature) is bytes:
            public_key.verify(
                signature,
                bytes(pre_hashed.encode('ascii')),
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH),
                hashes.SHA256())
            return True
        return False
    except InvalidSignature as e:
        print_message(ERROR_SIGNATURE_VERIFICATION, color=COLOR_RED, e=e)
        return False
