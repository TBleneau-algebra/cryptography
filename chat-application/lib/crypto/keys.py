# System import
import sys

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

# Library import
from lib.print import *

# Constant import
from constant.error import *
from constant.color import *


def private_key_generation():
    try:
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )
        return private_key
    except Exception as e:
        print_message(ERROR_PRIVATE_KEY_GENERATION, color=COLOR_RED, e=e)
        sys.exit()


def public_key_generation(private_key):
    try:
        public_key = private_key.public_key()
        return public_key
    except Exception as e:
        print_message(ERROR_PUBLIC_KEY_GENERATION, color=COLOR_RED, e=e)
        sys.exit()


def private_key_to_bytes(private_key):
    try:
        key = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )
        return key
    except Exception as e:
        print_message(ERROR_PRIVATE_KEY_GENERATION, color=COLOR_RED, e=e)
        sys.exit()


def public_key_to_bytes(public_key):
    try:
        key = public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        return key
    except Exception as e:
        print_message(ERROR_PUBLIC_KEY_GENERATION, color=COLOR_RED, e=e)
        sys.exit()


def read_public_key(content):
    try:
        public_key = serialization.load_pem_public_key(
            content,
            backend=default_backend()
        )
        return public_key
    except Exception as e:
        print_message(ERROR_PUBLIC_KEY_RECOVERY, color=COLOR_RED, e=e)
        return None


def read_private_key(content):
    try:
        private_key = serialization.load_pem_private_key(
            content,
            password=None,
            backend=default_backend()
        )
        return private_key
    except Exception as e:
        print_message(ERROR_PRIVATE_KEY_RECOVERY, color=COLOR_RED, e=e)
        return None
