# System import
import sys

# Library import
from lib.print import *
from lib.crypto.utils import *
from lib.crypto.signature import *
from lib.crypto.encryption import *

# Constant import
from constant.color import *
from constant.error import *


def send(_socket, message, _socket_list=None):
    if _socket_list is None:
        _socket_list = []

    try:
        if type(message) is not bytes:
            _socket.send(bytes(message, encoding="utf-8"))
        else:
            _socket.send(message)
    except Exception as e:
        print_message(ERROR_SOCKET_SEND, color=COLOR_RED, e=e)

        _socket.close()
        if len(_socket_list) > 0:
            _socket_list.remove(_socket)
        sys.exit()


def receive(_socket):
    return _socket.recv(4096)


def send_message(_socket, public_key_user, private_key, user, message, _socket_list=None):
    if _socket_list is None:
        _socket_list = []

    signature = signature_creation(private_key, message)
    message = create_message_from_info(user, message, signature)
    encrypted_message = encrypt_message(public_key_user, message)
    send(_socket, encrypted_message, _socket_list)
