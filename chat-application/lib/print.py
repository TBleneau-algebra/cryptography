# System import
import logging

# Constant import
from constant.color import *


def print_in_blue(message, end="\n"):
    print("\33[34m\33[1m" + message + "\33[0m", end=end)


def print_in_yellow(message, end="\n"):
    print("\33[32m\r\33[1m" + message + "\33[0m", end=end)


def print_in_red(message, end="\n"):
    print("\33[31m\33[1m" + message + "\33[0m", end=end)


def print_in_green(message, end="\n"):
    print("\033[35m\33[1m" + message + "\33[0m", end=end)


def print_message(message, color=None, e=None, end="\n"):
    funcdict = {
        COLOR_YELLOW: print_in_yellow,
        COLOR_PURPLE: print_in_green,
        COLOR_BLUE: print_in_blue,
        COLOR_RED: print_in_red,
        None: print
    }
    funcdict[color](message, end)

    if e is not None:
        logging.exception(e)


def print_prompt(prompt):
    print("\33[32m\r\33[1m > " + prompt + " \33[0m", end="")


def print_server_info(message, name, ip, process):
    print("Client (%s, %s) " % (ip, process), message, " [", name, "]")
