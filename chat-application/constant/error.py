ERROR_SERVER_CONNECTION = "Error occurred during the connection to the server"
ERROR_SERVER_PORT = "The server port must be numeric and must have at least 4 digits"
ERROR_SERVER_STOPPED = "\nServer stopped"

ERROR_CONFIGURATION = "Error occurred during the recovery of the configuration"
ERROR_ARGUMENT = "The number of arguments must not be less or greater than 1"
ERROR_DISCONNECTED = "\nYou have been disconnected"
ERROR_ADDRESS_ALREADY_USED = "This address is already used"

ERROR_SOCKET_SEND = "Error occurred during data sending via socket"

ERROR_SIGNATURE_CREATION = "Error occurred during the signature creation",
ERROR_SIGNATURE_VERIFICATION = "Error occurred during the signature verification"

ERROR_PRIVATE_KEY_GENERATION = "Error occurred during the private key generation"
ERROR_PUBLIC_KEY_GENERATION = "Error occurred during the public key generation"
ERROR_PUBLIC_KEY_RECOVERY = "Error occurred during the recovery of your public key"
ERROR_PRIVATE_KEY_RECOVERY = "Error occurred during the recovery of your private key"

ERROR_USERNAME_ALREADY_TAKEN = "Username already taken"

ERROR_ENCRYPTION = "Error occurred during the encryption process"
ERROR_DECRYPTION = "Error occurred during the decryption process"

ERROR_CONVERSION_BYTES = "Error occurred during the conversion into bytes"
ERROR_CONVERSION_STRING = "Error occurred during the conversion into string"
ERROR_CONVERSION_B64 = "Error occurred during the conversion into base64"
ERROR_CONVERSION_PUBLIC_KEY = "Error occurred during the conversion into public_key"
ERROR_CONVERSION_PRIVATE_KEY = "Error occurred during the conversion into private_key"
