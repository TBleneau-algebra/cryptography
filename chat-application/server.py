import socket
import select
import signal

# Library import
from lib.communication import *
from lib.crypto.signature import *
from lib.crypto.keys import *
from lib.crypto.utils import *
from lib.crypto.decryption import *

# Constant import
from constant.input import *
from constant.error import *
from constant.message import *
from constant.color import *

_server_name = "Admin"

_socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

private_key = private_key_generation()
public_key = public_key_generation(private_key)

public_key_bytes = public_key_to_bytes(public_key)
private_key_bytes = private_key_to_bytes(private_key)

public_key_signed = signature_creation(private_key, public_key_bytes)
private_key_signed = signature_creation(private_key, private_key_bytes)

_records_name = {}
_records_public_key = {}

connected_list = []


def send_to_all(_socket_user, message, user):
    for _socket_it in connected_list:
        if _socket_it != _socket_server and _socket_it != _socket_user:
            (i, p) = _socket_it.getpeername()
            send_message(_socket_it, _records_public_key[(i, p)], private_key,
                         string_to_bytes(user), string_to_bytes(message), connected_list)


def signal_close(sig, frame):
    send_to_all(_socket_server, ERROR_SERVER_STOPPED, _server_name)
    _socket_server.close()
    print_message(ERROR_SERVER_STOPPED, color=COLOR_RED)
    sys.exit()


def connection(host, port):
    try:
        _socket_server.bind((host, port))
        _socket_server.listen(10)
        connected_list.append(_socket_server)
        print_message(MESSAGE_SERVER_WORKING, color=COLOR_PURPLE)
    except Exception as e:
        print_message(ERROR_ADDRESS_ALREADY_USED, color=COLOR_RED, e=e)
        sys.exit()


def initialisation():
    try:
        host = input(INPUT_SERVER_ADDRESS)
        port = input(INPUT_SERVER_PORT)

        if not port.isnumeric():
            print_message(ERROR_SERVER_PORT, color=COLOR_RED)
            sys.exit()
        port = int(port)
    except Exception as e:
        print_message(ERROR_CONFIGURATION, color=COLOR_RED, e=e)
        sys.exit()

    return host, port


def handle_client_exit(_socket_user):
    (i, p) = _socket_user.getpeername()
    send_to_all(_socket_user, _records_name[(i, p)] + MESSAGE_USER_LEFT_CONVERSATION, _server_name)

    print_server_info(MESSAGE_USER_OFFLINE, _records_name[(i, p)], i, p)

    del _records_name[(i, p)]
    del _records_public_key[(i, p)]
    connected_list.remove(_socket_user)

    _socket_user.close()
    return


def handle_client_data(_socket_user):
    try:
        (i, p) = _socket_user.getpeername()
        data_received = receive(_socket_user)
        data_received = decrypt_message(private_key, data_received)

        user, message, signature = get_info_from_message(data_received)

        if not signature_validation(_records_public_key[(i, p)], message, signature):
            handle_client_exit(_socket_user)
        else:
            if bytes_to_string(message) == INPUT_EXIT:
                return handle_client_exit(_socket_user)
            else:
                send_to_all(_socket_user, bytes_to_string(message), bytes_to_string(user))

    except Exception as e:
        (i, p) = _socket_user.getpeername()
        send_to_all(_socket_user, _records_name[(i, p)] + MESSAGE_USER_LEFT_ERROR_CONVERSATION, _server_name)

        print_server_info(MESSAGE_USER_OFFLINE_ERROR, _records_name[(i, p)], i, p)

        del _records_name[(i, p)]
        del _records_public_key[(i, p)]
        connected_list.remove(_socket_user)

        _socket_user.close()
        return


def handle_new_connection():
    _socket_user, addr = _socket_server.accept()
    data_received = receive(_socket_user)

    user_name, message, signature = get_info_from_message(data_received)
    user_public_key = read_public_key(message)

    if not signature_validation(user_public_key, message, signature):
        _socket_user.close()
        return

    connected_list.append(_socket_user)
    _records_name[addr] = ""
    _records_public_key[addr] = None

    send_message(_socket_user, user_public_key, private_key, string_to_bytes(_server_name), public_key_bytes)

    confirmation_receive = receive(_socket_user)
    confirmation_receive = decrypt_message(private_key, confirmation_receive)

    _, confirmation, confirmation_signature = get_info_from_message(confirmation_receive)

    if not signature_validation(user_public_key, confirmation, confirmation_signature):
        _socket_user.close()
        return

    if bytes_to_string(confirmation) == MESSAGE_COMPLETE:

        if bytes_to_string(user_name) in _records_name.values() or bytes_to_string(user_name) == _server_name:
            send_message(_socket_user, user_public_key, private_key, string_to_bytes(_server_name),
                         string_to_bytes(ERROR_USERNAME_ALREADY_TAKEN))

            connected_list.remove(_socket_user)
            del _records_name[addr]
            del _records_public_key[addr]

            _socket_user.close()
            return
        else:
            (i, p) = _socket_user.getpeername()
            _records_name[(i, p)] = bytes_to_string(user_name)
            _records_public_key[(i, p)] = user_public_key

            send_message(_socket_user, user_public_key, private_key,
                         string_to_bytes(_server_name), string_to_bytes(MESSAGE_WELCOME))
            send_to_all(_socket_user, bytes_to_string(user_name) + MESSAGE_USER_JOINED_CONVERSATION, _server_name)

            print_server_info(MESSAGE_USER_CONNECTED, _records_name[addr], i, p)
    else:
        connected_list.remove(_socket_user)
        del _records_name[addr]
        del _records_public_key[addr]

        _socket_user.close()


def main():
    if len(sys.argv) != 1:
        print_message(ERROR_ARGUMENT, color=COLOR_RED)
        sys.exit()

    signal.signal(signal.SIGINT, signal_close)
    host, port = initialisation()

    connection(host, port)

    while 1:
        rlist, wlist, error_sockets = select.select(connected_list, [], [])

        for sock in rlist:
            if sock == _socket_server:
                handle_new_connection()
            else:
                handle_client_data(sock)


if __name__ == "__main__":
    main()
